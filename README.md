This is an authoritative DNS server that behaves as follows.  If the looked up host is v4-1-1-1-1.v6-2-2-2-2.crazysh.com:

- 1.1.1.1 is returned as the A record
- The IPv4 embedded IPv6 ::ffff:2.2.2.2 is returned as the AAAA record

This strange behavior was required for an SSRF exploit to work and might be useful for testing. A live example of this repository lives on crazysh.com 

Contact: clint@wtfismyip.com
